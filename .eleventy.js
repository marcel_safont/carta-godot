module.exports = function(config){
    config.addPassthroughCopy('src/site/xmls');
    config.addPassthroughCopy('src/site/js');
    config.addPassthroughCopy('src/site/css');
    config.addPassthroughCopy('src/site/svg');
    config.addPassthroughCopy('src/site/fonts');
    config.addCollection('pages', collection => {
        return collection.getFilteredByGlob('src/site/pages/*.md');
    });
    return {
        passthroughCopy: true,
        markdownTemplateEngine: "njk",
        templateFormats: ["html", "njk", "md"],
        dir: {
            input: "src/site",
            output: "dist",
            includes: "templates",
        }
    }
};