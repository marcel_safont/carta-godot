function fetchXml(url, renderDisplay){
	fetch(url)
	.then(res => res.text())
	.then(data => {
		let parser = new DOMParser();
		let xmlDoc = parser.parseFromString(data, 'text/xml');
		let items = Array.from(xmlDoc.getElementsByTagName('item'));
		let intro = Array.from(xmlDoc.getElementsByTagName('element'));
		let subfamilies = Array.from(xmlDoc.getElementsByTagName('subfamilia'));

		intro.forEach(item => {
			if(renderDisplay == 'products'){
				renderIntro(item);
			}
		})

		if(subfamilies.length > 0 ){
			subfamilies.forEach(item => {
				let items = renderSubfamilia(item);
				document.querySelector('#dinamic-content').appendChild(items);
			})
		}else{
			items.forEach((item, index, list) => {
				if(renderDisplay == 'home'){
					renderListHome(item);
				}else if(renderDisplay == 'products'){
					let items = renderListProducts(item, index, list);
					document.querySelector('#dinamic-content').appendChild(items);
				}
			})
		}	
		
	}).then(() => {
		const dinamic_height = (window.innerHeight - 90) - (document.querySelector('#languages').offsetHeight + document.querySelector('#intro').offsetHeight);
		document.querySelector('#dinamic-content').style.height = dinamic_height+'px';
		OverlayScrollbars(document.querySelector("#dinamic-content"), { 
			className       : "os-theme-dark",
			paddingAbsolute : true,
			scrollbars : {
				clickScrolling : true
			}
		});
	})
	.catch(error => {
		console.log(`Hubo un problema con la petición Fetch: ${error.message}`);
	});
}

function renderListHome(item){
	let linkEl = document.createElement('a');
	let divTitle = document.createElement('div');
	if(item.getAttribute('highlight') == 'true'){
		divTitle.setAttribute('class', 'highlight');
	}
	let divElement = document.createElement('div');
	linkEl.setAttribute('href', item.getElementsByTagName('link')[0].textContent);
	let nodeText = document.createTextNode(item.getElementsByTagName('title')[0].textContent);
	linkEl.appendChild(nodeText);
	divTitle.appendChild(nodeText);
	divElement.appendChild(divTitle);
	divElement.appendChild(linkEl);
	document.querySelector('#dinamic-content').appendChild(divElement);
}

function renderIntro(item){
	let divIntro = document.createElement('div');
	let divInfo = document.createElement('div');
	let divMenuPrice = document.createElement('div');
	
	if(item.getElementsByTagName('menu-price').length > 0){
		let nodeMenuPriceText = item.getElementsByTagName('menu-price')[0].textContent;
		divMenuPrice.append(nodeMenuPriceText);
		divMenuPrice.setAttribute('class', 'menu-price');
		document.querySelector('#intro').append(divMenuPrice);
	}
	divIntro.setAttribute('class', 'title');
	divInfo.setAttribute('class', 'info');
	let nodeText = document.createTextNode(item.getElementsByTagName('title')[0].textContent);
	divIntro.appendChild(nodeText);
	infoText = item.getElementsByTagName('p');
	Array.from(infoText).forEach(el => {
		let p = document.createElement('p');
		if(el.getAttribute('icon')){
			p.setAttribute('class', el.getAttribute('icon'));
		}
		let pText = document.createTextNode(el.textContent);
		p.appendChild(pText);
		divInfo.appendChild(p);
	})
	document.querySelector('#intro .content').appendChild(divIntro);
	document.querySelector('#intro .content').appendChild(divInfo);
}

function renderSubfamilia(item){
	let divMain = document.createElement('div');
	let divProduct = document.createElement('div');
	let divTitleFam = document.createElement('div');
	divTitleFam.innerHTML = item.getElementsByTagName('title-family')[0].innerHTML;
	if(item.getElementsByTagName('description-family').length > 0){
		divFamDescr = document.createElement('span');
		divFamDescr.innerHTML += item.getElementsByTagName('description-family')[0].innerHTML;
		divTitleFam.append(divFamDescr);
	}
	
	divMain.setAttribute('class', 'family');
	divTitleFam.setAttribute('class', 'title-family');
	divProduct.setAttribute('class', 'main-product');
	divMain.appendChild(divTitleFam);
	if(item.getElementsByTagName('item').length > 0){
		let familyRender = Array.from(item.getElementsByTagName('item')).forEach((el, index, list) => {
			let element = renderListProducts(el, index, list);
			divMain.appendChild(element);
		})
	}
	return divMain;
}

function renderListProducts(item, index, list){
	let is_double_price = false;
	Array.from(list).forEach(item =>{	
		if(item.getAttribute('showtitles') == 'true'){
			document.querySelector('#page').classList.add('list-titles');
		}
		if(item.getElementsByTagName('price2').length > 0){
			document.querySelector('#page').classList.add('double-price');
			is_double_price =  true;
		}
	})
	//main divs
	let divMain = document.createElement('div');
	let divProperties = document.createElement('div');
	let divElInfo = document.createElement('div');
	let divElTtitle = document.createElement('div');
	let divElDesc = document.createElement('div');
	let divPrices = document.createElement('div');
	let divPrice1 = document.createElement('div');
	let divPrice2 = document.createElement('div');

	divMain.setAttribute('class', 'main-product');
	if(item.getAttribute('info') == 'true'){
		divMain.setAttribute('class', 'main-product info');
	}
	if(item.getAttribute('price') == 'true'){
		divMain.setAttribute('class', 'main-product price');
	}
	if(item.getAttribute('note') == 'true'){
		divMain.setAttribute('class', 'main-product note');
	}
	if(item.getAttribute('option') == 'true'){
		divMain.setAttribute('class', 'main-product option');
	}

	divElTtitle.setAttribute('class', 'title');
	divElDesc.setAttribute('class', 'descr');
	divPrices.setAttribute('class', 'prices');
	divProperties.setAttribute('class', 'attributes');
	//properties conditional
	if(item.getElementsByTagName('glutenfree').length > 0){
		let divGluten = document.createElement('div');
		divGluten.setAttribute('class', 'glutenfree');
		divProperties.appendChild(divGluten);
	}
	if(item.getElementsByTagName('vegetarian').length > 0){
		let divVeggy = document.createElement('div');
		divVeggy.setAttribute('class', 'vegetarian');
		divProperties.appendChild(divVeggy);
	}
	if(item.getElementsByTagName('big').length > 0){
		let divBig = document.createElement('div');
		divBig.setAttribute('class', 'big');
		divProperties.appendChild(divBig);
	}

	let nodeText = document.createTextNode(item.getElementsByTagName('title')[0].textContent);
	divElTtitle.appendChild(nodeText);


	if(item.getElementsByTagName('description').length > 0){
		let nodeDesc = document.createTextNode(item.getElementsByTagName('description')[0].textContent);
		divElDesc.appendChild(nodeDesc);
	}else{
		divElDesc.innerHTML = '';
	}
	if(item.getElementsByTagName('price1').length > 0 ){
		
		let nodePrice1 = document.createTextNode(item.getElementsByTagName('price1')[0].textContent);
		if(item.getAttribute('oneprice') == "true"){
			divPrice1.setAttribute('class', 'oneprice');
		}
	
		divPrice1.appendChild(nodePrice1);
		divPrices.appendChild(divPrice1);
	}
	if(item.getElementsByTagName('price2').length > 0 ){
		let nodePrice2 = document.createTextNode(item.getElementsByTagName('price2')[0].textContent);
		divPrice2.appendChild(nodePrice2);
		divPrices.appendChild(divPrice2);
	}

	//append divs to main div
	
	
	divElInfo.appendChild(divElTtitle);
	divElInfo.appendChild(divElDesc);
	if(is_double_price && index == 0){
		try{
			const item = document.querySelector('#intro');
			item.querySelector('.column-titles').remove();
		}catch(e){
			console.log(e);
		}
		
		let columnsTitle = document.createElement('div');
		columnsTitle.setAttribute('class', 'column-titles');
		columnsTitle.innerHTML = '<div>Copa</div><div>Ampolla</div>';
		//columnsTitle.appendChild(document.createTextNode('hola'));
		document.querySelector('#intro').appendChild(columnsTitle);
	}
	divMain.appendChild(divProperties);
	divMain.appendChild(divElInfo);
	divMain.appendChild(divPrices);
	return divMain;
	//document.querySelector('#dinamic-content').appendChild(divMain);
}



